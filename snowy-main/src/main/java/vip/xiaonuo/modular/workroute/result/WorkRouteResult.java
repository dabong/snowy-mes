package vip.xiaonuo.modular.workroute.result;

import lombok.Data;
import vip.xiaonuo.modular.workroute.entity.WorkRoute;
import vip.xiaonuo.modular.worksteproute.entity.WorkStepRoute;

import java.util.List;

@Data
public class WorkRouteResult extends WorkRoute{
    List<WorkStepRoute> workStepRouteList ;

}
