import { axios } from '@/utils/request'

/**
 * 查询供应商分类
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function suppSortPage (parameter) {
  return axios({
    url: '/suppSort/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 供应商分类列表
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function suppSortList (parameter) {
  return axios({
    url: '/suppSort/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加供应商分类
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function suppSortAdd (parameter) {
  return axios({
    url: '/suppSort/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑供应商分类
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function suppSortEdit (parameter) {
  return axios({
    url: '/suppSort/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除供应商分类
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function suppSortDelete (parameter) {
  return axios({
    url: '/suppSort/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出供应商分类
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function suppSortExport (parameter) {
  return axios({
    url: '/suppSort/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 获取类型树
 *
 * @author ZJK
 * @date 2022-08-04 17:01:21
 */
export function getSuppTree (parameter) {
  return axios({
    url: '/suppSort/tree',
    method: 'get',
    params: parameter
  })
}
